const { mouse, Point } = require("@nut-tree/nut-js");

(async () => {
    const target = new Point(500, 350);
    await mouse.setPosition(target);
})();
