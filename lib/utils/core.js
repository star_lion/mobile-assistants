const coreUtils = {}

coreUtils.arrayCycleForwards = (array) => array.push(array.shift())
coreUtils.arrayCycleBackwards = (array) => array.unshift(array.pop())
coreUtils.getNextIndexFromIndex = (currentIndex, array) => (currentIndex == array.length - 1) ? 0 : currentIndex + 1
coreUtils.getPreviousIndexFromIndex = (currentIndex, array) => (currentIndex == 0) ? array.length - 1 : currentIndex - 1
coreUtils.sleepMs = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

module.exports = coreUtils
