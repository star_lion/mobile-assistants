const {mouse, sleep, Region} = require('@nut-tree/nut-js')

const nutJsUtils = {}

nutJsUtils.executeEventSequence = async (events=[], logFn=(() => {})) => {
  for (const [point=undefined, shouldClick=false, postDelayMs=500, log='.'] of events) {
    logFn(log)
    if (point != undefined) await mouse.setPosition(point)
    if (shouldClick == true) await mouse.leftClick()
    await sleep(postDelayMs)
  }
}

nutJsUtils.scaleRegion = (region, scaleFactor=1, xAnchorDirection, yAnchorDirection) => {
  if (region == undefined) throw new Error('scaleRegion requires region')
  const scaledHeight = region.height * scaleFactor
  const scaledWidth = region.width * scaleFactor
  let newLeft, newTop

  if (xAnchorDirection == 0) {
    // left
    newLeft = region.left
  } else if (xAnchorDirection == 1) {
    // right
    newLeft = region.left + (region.width - scaledWidth)
  } else {
    // center
    newLeft = region.left + (region.width - scaledWidth)/2
  }

  if (yAnchorDirection == 0) {
    // top
    newTop = region.top
  } else if (yAnchorDirection == 1) {
    // bottom
    newTop = region.top + (region.height - scaledHeight)
  } else {
    // center
    newTop = region.top + (region.height - scaledHeight)/2
  }

  return new Region(newLeft, newTop, scaledWidth, scaledHeight)
}

nutJsUtils.scaleRegionHalf = (region, xAnchorDirection, yAnchorDirection) => nutJsUtils.scaleRegion(region, .5, xAnchorDirection, yAnchorDirection)

module.exports = nutJsUtils
