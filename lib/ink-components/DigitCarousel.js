'use strict'
const React = require('react')
const {Box, Text, useFocus, useInput} = require('ink')

const {useState} = React

const DigitCarousel = ({
  id,
  value=5,
  isActive=true,
  onRight=()=>{},
  onLeft=()=>{},
  ...rest
}) => {
  const {isFocused} = useFocus({id})

  useInput((input, key) => {
    if (isFocused && key.leftArrow) onLeft()
    if (isFocused && key.rightArrow) onRight()
  })

  return (
    <Box {...rest}>
      <Text color={isFocused ? 'green' : 'white'}>
        {isFocused
          ? `← ${value} →`
          : `${value}`
        }
      </Text>
    </Box>
  )
}

module.exports = DigitCarousel
