const React = require('react')
const util = require('util')
const {Box, Text, useInput, useFocus, useFocusManager} = require('ink')
const FocusContext = require('ink/build/components/FocusContext').default
const importJsx = require('import-jsx')
const ArrowFocusManager = importJsx('./ArrowFocusManager')

const {useState, useContext} = React

const CommandBar = ({children, ...rest}) => {
  return (
    <Box
      {...rest}
    >
      <ArrowFocusManager verticalControl={true} horizontalControl={true}>
        {children}
      </ArrowFocusManager>
    </Box>
  )
}

module.exports = CommandBar
