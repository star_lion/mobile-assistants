'use strict'
const React = require('react')
const {Text, Box, Newline, useFocus, useInput} = require('ink')
const importJsx = require('import-jsx')

const {useState} = React

const AttentionContainer = ({autoFocus=false, children}) => {
  const {isFocused} = useFocus({autoFocus})
  return (
    <Box
      border={0}
      borderStyle='round'
      borderColor={isFocused ? 'white' : 'hidden'}
    >
      {children}
    </Box>
  )
}

module.exports = AttentionContainer
