const React = require('react')
const {Box, Text, useFocus} = require('ink')
const FocusContext = require('ink/build/components/FocusContext').default
const importJsx = require('import-jsx')
const {uIOhook, UiohookKey} = require('uiohook-napi')
const activeWindow = require('active-win')
const uuid = require('uuid').v4

const {useState, useEffect, useContext} = React

// TODO: further improve currentWindow checking by explicitly taking window title as prop
// TODO: add press highlight animation with some state that changes off an interval
const Command = ({
  id,
  autoFocus,
  name='Command',
  prohibitedFocusIds=[],
  keyValidator=()=>false,
  global=false,
  onRun=()=>{},
  hotkeysStore
}) => {
  if (hotkeysStore == undefined) {throw new Error('Command component requires a hotkeysStore')}
  const {isFocused, focus} = useFocus({id, autoFocus})
	const activeFocusId = useContext(FocusContext).activeId
  const isEnabled = !prohibitedFocusIds.includes(activeFocusId)
  let setId
  [id, setId] = useState(id || uuid())
  // if (id == undefined) {setId(uuid())}

  // useEffect(() => {
  //   const callback = async (event) => {
  //     if (
  //       (isEnabled && keyValidator(event)) ||
  //       (isEnabled && isFocused && event.keycode == UiohookKey.Enter)
  //     ) {

  //       const currentWindow = await activeWindow()
  //       if (global == true) onRun()
  //       else if (global == false && currentWindow && ['pogo', 'pmex'].includes(currentWindow.title)) onRun()
  //     }
  //   }
  //   const eventEmitter = uIOhook.on('keydown', callback)
  //   return () => eventEmitter.removeListener('keydown', callback)
  // }, [isFocused, isEnabled])

  const hotkeysStoreUpdate = hotkeysStore.getState().update
  useEffect(() => {
    if (id == undefined) return
    if (isEnabled) {
      // console.log(`attach ${id}`)
      hotkeysStoreUpdate((state) => {
        state.hotkeys[id] = {
          callback: async (event) => {
            if (
              keyValidator(event) ||
              isFocused && event.keycode == UiohookKey.Enter
            ) {
              const currentWindow = await activeWindow()
              if (global == true) onRun()
              else if (global == false && currentWindow && ['pogo', 'pmex'].includes(currentWindow.title)) onRun()
            }
          },
        }
      })
    } else {
      hotkeysStoreUpdate((state) => {
        // console.log(`detach ${id}`)
        delete state.hotkeys[id]
      })
    }
    return () => hotkeysStoreUpdate((state) => {
      // console.log(`detach ${id}`)
      delete state.hotkeys[id]
    })

  }, [id, activeFocusId, isFocused, isEnabled])

  return (
      <Box borderStyle='round' borderColor={isFocused ? 'green' : 'white'}>
        <Text bold={isFocused}>{name}</Text>
      </Box>
  )
}

module.exports = Command
