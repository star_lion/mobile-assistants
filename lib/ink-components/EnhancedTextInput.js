const React = require('react')
const {Text, useFocus} = require('ink')
const importJsx = require('import-jsx')
const TextInput = importJsx('ink-text-input').default

const EnhancedTextInput = ({id, value='', onChange=() => {}}) => {
  const {isFocused} = useFocus({id})

  return (
    <>
      {isFocused
        ? <Text bold={isFocused} color={isFocused ? 'green' : 'white'}><TextInput value={value} onChange={onChange} focus={isFocused} /></Text>
        : <Text>{value}</Text>
      }
    </>
  )
}

module.exports = EnhancedTextInput
