'use strict'
const React = require('react')
const {Text, Box} = require('ink')

const {useEffect} = React
const {uIOhook, UiohookKey} = require('uiohook-napi')

const defaultKeyValidator = (event) => event.altKey == true && event.keycode == UiohookKey.Q

const TitleBar = ({title='Title', keyValidator=defaultKeyValidator, quitText='[Alt-Q]uit'}) => {
  useEffect(() => {
    // TODO cleanup
    uIOhook.on('keydown', (event) => {
      if (keyValidator(event)) process.exit(0)
    })
    return () => {}
  }, [])

  return (
    <Box
      width='100%'
      justifyContent='space-between'
    >
      <Text>{title}</Text>
      <Text>{quitText}</Text>
    </Box>
  )
}

module.exports = TitleBar
