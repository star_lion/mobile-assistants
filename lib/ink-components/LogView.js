const React = require('react')
const {Box, Text} = require('ink')
const {createStore} = require('@colorfy-software/zfy')
const importJsx = require('import-jsx')
const {inspect} = require('util')

const Column = importJsx('./column')

const initialState = {
  logs: []
}

const logStore = createStore('logs', initialState)

const log = (text) => {
  if (text == undefined) return
  logStore.getState().update(data => (
    data.logs.push(`[${new Date().toLocaleTimeString()}] ${text}`)
  ))
}

const logInspect = (object) => {log(inspect(object))}

const logInPlace = (text) => {
  if (text == undefined) return
  logStore.getState().update(data => {
    if (data.logs.length == 0) {
      data.logs.push(`[${new Date().toLocaleTimeString()}] ${text}`)
    } else {
      data.logs[data.logs.length - 1] += text
    }
  })
}

log(`Log store created`)

const LogView = ({maxDisplayCount=15}) => {
  const logs = logStore(store => store.data.logs)
  return (
    <Column>
      {logs.slice(maxDisplayCount * -1).map((logText, index) => (
        <Text key={`l${index}`}>{logText}</Text>
      ))}
    </Column>
  )
}

module.exports = {
  LogView,
  log,
  logInspect,
  logInPlace
}
