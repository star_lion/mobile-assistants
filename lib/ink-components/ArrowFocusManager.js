const React = require('react')
const util = require('util')
const {Box, Text, useInput, useFocus, useFocusManager} = require('ink')
const FocusContext = require('ink/build/components/FocusContext').default

const {useContext} = React

const ArrowFocusManager = ({children, verticalControl=true, horizontalControl=false, focusOutOfBounds=true}) => {
  const {focusNext, focusPrevious} = useFocusManager()
	const activeFocusId = useContext(FocusContext).activeId
  const childrenFocusIds = children.map((child) => child.props.id)
  const focusedChildIndex = childrenFocusIds.indexOf(activeFocusId)
  const hasAnyFocusedChildren = childrenFocusIds.includes(activeFocusId)

  useInput((input, key) => {
    // TODO: implement focusOutOfBounds option
    if (verticalControl && key.upArrow && hasAnyFocusedChildren) focusPrevious()
    if (verticalControl && key.downArrow && hasAnyFocusedChildren) focusNext()
    if (horizontalControl && key.leftArrow && hasAnyFocusedChildren) focusPrevious()
    if (horizontalControl && key.rightArrow && hasAnyFocusedChildren) focusNext()
  })

  return (
    <>
      {children}
    </>
  )
}

module.exports = ArrowFocusManager
