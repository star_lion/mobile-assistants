'use strict'
const React = require('react')
const {Box, Text, useFocus, useInput} = require('ink')

const TextCarousel = ({
  id,
  autoFocus,
  isActive=true,
  values=['One', 'Two', 'Three'],
  onRight=()=>{},
  onLeft=()=>{},
  ...rest
}) => {
  const {isFocused} = useFocus({id, autoFocus, isActive})

  useInput((input, key) => {
    if (isFocused && key.rightArrow) onRight()
    if (isFocused && key.leftArrow) onLeft()
  })

  return (
    <Box {...rest}>
      <Text color={isFocused ? 'green' : 'white'}>
        {isFocused
          ? `← ${values[0]} →`
          : `${values[0]}`
        }
      </Text>
    </Box>
  )
}

module.exports = TextCarousel
