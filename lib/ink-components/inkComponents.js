const importJsx = require('import-jsx')

module.exports = {
  TitleBar: importJsx('./TitleBar'),
  Column: importJsx('./Column'),
  TabBar: importJsx('./TabBar'),
  AttentionContainer: importJsx('./AttentionContainer'),
  EnhancedTextInput: importJsx('./EnhancedTextInput'),
  CommandBar: importJsx('./CommandBar'),
  Command: importJsx('./Command'),
  DigitCarousel: importJsx('./DigitCarousel'),
  TextCarousel: importJsx('./TextCarousel'),
  ArrowFocusManager: importJsx('./ArrowFocusManager'),
  LogView: importJsx('./LogView').LogView
}
