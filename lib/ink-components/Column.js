const React = require('react')
const {Box} = require('ink')

const Column = ({children, ...rest}) => (
  <Box
    flexDirection='column'
    {...rest}
  >
    {children}
  </Box>
)

module.exports = Column
