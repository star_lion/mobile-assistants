'use strict'
const React = require('react')
const {Text, Box, useFocus, useInput} = require('ink')

const {useState} = React

const Tab = ({tabName, tabIndex, activeTabIndex}) => {
  return tabIndex == activeTabIndex ? (
    <Box borderStyle='single' border={1}>
      <Text bold={true} color='green'>{' < '}{tabName}{' > '}</Text>
    </Box>
  ) : (
    <Box marginX={1}>
      <Text color='gray'>{tabName}</Text>
    </Box>
  )
}

const TabBar = ({
  tabNames=['Tab 1', 'Tab 2', 'Tab 3'],
  activeTabIndex=0,
  onNavigateRight=()=>{},
  onNavigateLeft=()=>{}
}) => {
  // const [activeTabIndex, setActiveTabIndex] = useState(defaultTabIndex)

  useInput((input, key) => {
    // let newTabIndex
    if (input == '>') onNavigateRight()
    if (input == '<') onNavigateLeft()
    // if (['<','>'].includes(input)) {
    //   setActiveTabIndex(newTabIndex)
    // }
  })

  return (
    <Box alignItems='center'>
      {tabNames.map((tabName, index) => (
        <Tab key={`t${index}`} tabName={tabName} tabIndex={index} activeTabIndex={activeTabIndex} />
      ))}
    </Box>
  )
}

module.exports = TabBar
