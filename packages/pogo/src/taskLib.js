const {
  getWindows,
  screen,
  mouse,
  Point,
  straightTo,
  OptionalSearchParameters,
  randomPointIn,
  sleep,
  centerOf,
} = require("@nut-tree/nut-js");
const importJsx = require("import-jsx");
const { log, logInspect, logInPlace } = importJsx("ink-components/LogView");
const {
  arrayCycleForwards,
  nutJs: { scaleRegionHalf },
} = require("utils");
const R = require("ramda");

const { stores } = require("./useStores");
const uiImages = require("./uiImages");

const { path } = R;

const configureNutJs = () => {
  screen.config.highlightDurationMs = 100;
  screen.config.autoHighlight = true;
  mouse.config.mouseSpeed = 4000;
};
configureNutJs();

const taskLib = {};
taskLib.getDevices = async () => {
  const pogoStore = stores.pogo.getState();
  const devices = pogoStore.data.devices;
  const windows = await getWindows();
  const [windowTitles, windowRegions] = await Promise.all([
    Promise.all(windows.map((window) => window.title)),
    Promise.all(windows.map((window) => window.region)),
  ]);

  // logInspect(windowTitles)

  const d1Index = R.findIndex(R.equals(devices[0].title), windowTitles);
  if (d1Index != -1) {
    screen.highlight(windowRegions[d1Index]);
    pogoStore.update((state) => {
      state.devices[0].window = windows[d1Index];
      state.devices[0].region = windowRegions[d1Index];
    });
  } else {
    pogoStore.update((state) => {
      state.devices[0].window = undefined;
      state.devices[0].region = undefined;
    });
  }

  const d2Index = R.findIndex(R.equals(devices[1].title), windowTitles);
  if (d2Index != -1) {
    screen.highlight(windowRegions[d2Index]);
    pogoStore.update((state) => {
      state.devices[1].window = windows[d2Index];
      state.devices[1].region = windowRegions[d2Index];
    });
  } else {
    pogoStore.update((state) => {
      state.devices[1].window = undefined;
      state.devices[1].region = undefined;
    });
  }
};

taskLib.storeDataCycle = (storeName, storeDataPath, cycleFn) => {
  stores[storeName].getState().update((data) => {
    cycleFn(path(storeDataPath, data));
  });
};

taskLib.increaseTradeCount = () =>
  stores.trade.getState().update((state) => {
    if (state.count < 100) state.count++;
  });
taskLib.decreaseTradeCount = () =>
  stores.trade.getState().update((state) => {
    if (state.count > 0) state.count--;
  });

taskLib.highlightDevices = async () => {
  const { devices } = stores.pogo.getState().data;
  const d1Region = devices && devices[0].region;
  const d2Region = devices && devices[1].region;
  if (d1Region) await screen.highlight(d1Region);
  if (d2Region) await screen.highlight(d2Region);
};

const randomPointNearCenter = (
  region,
  randomizeX = true,
  randomizeY = true,
  extent = 0
) => new Point(0, 0);

const tradeStep = (stepFn) => (args) => {
  taskLib.storeDataCycle("trade", ["steps"], arrayCycleForwards);
  return stepFn(args);
};

const searchParamsFromDevices = R.memoizeWith(R.identity, (devices) => {
  return R.compose(
    R.map((region) => new OptionalSearchParameters(region, 0.96, true)),
    R.map(R.prop("region"))
  )(devices);
});

taskLib.openTradeView = tradeStep(async (devices = []) => {
  const searchParams = searchParamsFromDevices(devices);

  log("Opening trade view");
  const tradeControlRegions = await Promise.all(
    R.map((searchParams) =>
      screen.waitFor(
        uiImages.trade.friendControlsTrade,
        2000,
        200,
        searchParams
      )
    )(searchParams)
  );

  for (const region of tradeControlRegions) {
    const randomPoint = await randomPointIn(scaleRegionHalf(region));
    logInPlace(`, (${randomPoint.x}, ${randomPoint.y})`);
    await mouse.setPosition(randomPoint);
    await mouse.leftClick();
  }
});

taskLib.selectMon = tradeStep(async (devices = []) => {
  const searchParams = searchParamsFromDevices(devices);

  log("Selecting pokemon");
  const healthBarRegions = await Promise.all(
    R.map((searchParams) =>
      screen.waitFor(
        uiImages.trade.healthBar,
        2000,
        200,
        R.modify(
          "searchRegion",
          (searchRegion) => scaleRegionHalf(searchRegion, 0, 0),
          searchParams
        )
      )
    )(searchParams)
  );

  for (const region of healthBarRegions) {
    const centerPoint = await centerOf(region);
    logInPlace(`, (${centerPoint.x}, ${centerPoint.y})`);
    await mouse.setPosition(centerPoint);
    await mouse.leftClick();
  }
});

taskLib.confirmMon = tradeStep(async (devices = []) => {
  const searchParams = searchParamsFromDevices(devices);

  log("Confirming pokemon");
  const nextButtonRegions = await Promise.all(
    R.map((searchParams) =>
      screen.waitFor(uiImages.trade.nextButton, 2000, 200, searchParams)
    )(searchParams)
  );

  for (const region of nextButtonRegions) {
    const randomPoint = await randomPointIn(scaleRegionHalf(region));
    logInPlace(`, (${randomPoint.x}, ${randomPoint.y})`);
    await mouse.setPosition(randomPoint);
    await mouse.leftClick();
  }
});

taskLib.confirmTrade = tradeStep(async (devices = []) => {
  const searchParams = searchParamsFromDevices(devices);

  log("Confirming trade");
  const confirmButtonRegions = await Promise.all(
    R.map((searchParams) =>
      screen.waitFor(uiImages.trade.confirmButton, 2000, 200, searchParams)
    )(searchParams)
  );

  for (const region of confirmButtonRegions) {
    const randomPoint = await randomPointIn(scaleRegionHalf(region));
    logInPlace(`, (${randomPoint.x}, ${randomPoint.y})`);
    await mouse.setPosition(randomPoint);
    await mouse.leftClick();
  }
});

taskLib.closeMon = tradeStep(async (devices = []) => {
  const searchParams = searchParamsFromDevices(devices);

  log("Closing pokemon");
  const menuCloseRegions = await Promise.all(
    R.map((searchParams) =>
      screen.waitFor(
        uiImages.general.menuCloseGreen,
        2000,
        200,
        R.assoc("confidence", 0.95, searchParams)
      )
    )(searchParams)
  );

  for (const region of menuCloseRegions) {
    const randomPoint = await randomPointIn(scaleRegionHalf(region));
    logInPlace(`, (${randomPoint.x}, ${randomPoint.y})`);
    await mouse.setPosition(randomPoint);
    await mouse.leftClick();
  }
});

taskLib.startTrading = async () => {
  const tradeStore = stores.trade;
  const pogoState = stores.pogo.getState().data;
  const tradeState = tradeStore.getState().data;

  try {
    // get devices or single preferred device
    const selectedDevices =
      tradeState.deviceModes[0] == "Single"
        ? [pogoState.devices[0]] // TODO: make preferred device
        : [pogoState.devices[0], pogoState.devices[1]];

    if (tradeState.executionModes[0] == "Manual") {
      const manualStep = tradeState.steps[0];
      await taskLib[manualStep.taskName](selectedDevices);
    } else {
      if (tradeState.count == 0) throw new Error("Trade count is 0");

      const remainingSteps = R.take(
        tradeState.steps.length - tradeState.steps[0].number + 1
      )(tradeState.steps);
      const orderedSteps = R.sortBy(R.prop("number"))(tradeState.steps);

      log("Running remaining cycle");
      for (const step of remainingSteps) {
        await taskLib[step.taskName](selectedDevices);
        // log(step.animationTimes.slow)
        await sleep(step.animationTimes.slow * 1000);
      }
      taskLib.decreaseTradeCount();

      log("Running complete cycles");
      while (tradeStore.getState().data.count > 0) {
        for (const step of orderedSteps) {
          await taskLib[step.taskName](selectedDevices);
          // log(step.animationTimes.slow)
          await sleep(step.animationTimes.slow * 1000);
        }
        taskLib.decreaseTradeCount();
      }
      log("Trading complete");
    }
  } catch (error) {
    log(error);
  }
};

taskLib.openGiftRecurse = async (searchParams, giftRegion) => {
  log("Open gift and recurse");
  if (giftRegion == undefined) {
    logInPlace(", checking gift,");
    const [receiveGiftSettlement] = await Promise.allSettled([
      screen.waitFor(uiImages.gift.receive, 4000, 200, searchParams),
    ]);
    if (receiveGiftSettlement.status == "rejected") {
      log("ERROR: Failed to recognize gift, CANCELLING");
      return;
    }
    giftRegion = receiveGiftSettlement.value;
  }

  await mouse.setPosition(await randomPointIn(giftRegion));
  await mouse.leftClick();
  logInPlace(".");
  await sleep(4000);
  const [giftOpenSettlement, menuCloseSettlement] = await Promise.allSettled([
    screen.waitFor(uiImages.gift.open, 1000, 200, searchParams),
    screen.waitFor(uiImages.general.menuCloseWhite, 1000, 200, searchParams),
  ]);
  if (giftOpenSettlement.status == "rejected") {
    log("ERROR: Could not find OPEN button, CANCELLING");
    return;
  }

  logInPlace(".");
  await mouse.setPosition(await randomPointIn(giftOpenSettlement.value));
  await mouse.leftClick();

  await sleep(200);

  const [inventoryFullSettlement, dailyLimitSettlement] =
    await Promise.allSettled(
      [uiImages.general.inventoryFull, uiImages.gift.receiveLimitMessage].map(
        (image) =>
          screen.waitFor(
            image,
            1000,
            200,
            R.assoc("confidence", 0.95, searchParams)
          )
      )
    );

  if (inventoryFullSettlement.status == "fulfilled") {
    log("ERROR: Inventory is full, CANCELLING");
    return;
  }

  if (dailyLimitSettlement.status == "fulfilled") {
    log("ERROR: Daily limit has been reached, CANCELLING");
    return;
  }

  if (menuCloseSettlement.status == "fulfilled") {
    await mouse.setPosition(await randomPointIn(menuCloseSettlement.value));
    await sleep(400);
    const loopCount = 1;
    for (let i = 0; i < loopCount; i++) {
      await sleep(100);
      await mouse.leftClick();
    }
  }

  // allow for full animation in case close button wasn't spammed
  const [friendControlsSettlement] = await Promise.allSettled([
    screen.waitFor(uiImages.general.friendControls, 14000, 200, searchParams),
  ]);

  if (friendControlsSettlement.status == "rejected") {
    log("ERROR: Could not find friend controls to swip with, CANCELLING");
    return;
  }

  logInPlace(".");
  const friendControlsRegion = friendControlsSettlement.value;
  const { left, top, width, height } = friendControlsRegion;
  const rightSwipePoint = new Point(
    left + width,
    top + Math.floor(Math.random() * height)
  );
  const leftSwipePoint = new Point(
    left,
    top + Math.floor(Math.random() * height)
  );

  await mouse.setPosition(rightSwipePoint);
  await mouse.drag(straightTo(leftSwipePoint));
  taskLib.openGiftRecurse(searchParams);
};

taskLib.receiveGifts = async () => {
  const pogoState = stores.pogo.getState().data;
  const deviceRegion = pogoState.devices[0].region;

  log("Receiving gifts, ");
  logInPlace("checking for gift or friend profile");
  const searchParams = new OptionalSearchParameters(deviceRegion, 0.95, true);

  const [receiveGiftSettlement, friendControlsSettlement] =
    await Promise.allSettled(
      [uiImages.gift.receive, uiImages.general.friendControls].map(
        (imageTarget) => screen.waitFor(imageTarget, 2000, 200, searchParams)
      )
    );
  const receiveGiftSearchFulfilled =
    receiveGiftSettlement.status == "fulfilled";
  const friendControlsSearchFulfilled =
    friendControlsSettlement.status == "fulfilled";
  if (receiveGiftSearchFulfilled) {
    log("Gift recognized, open");
    taskLib.openGiftRecurse(searchParams, receiveGiftSettlement.value);
  } else if (friendControlsSearchFulfilled) {
    log("Friend profile recognized, swipe, and check gift again");
  } else {
    log("ERROR: Neither gift nor friend profile recognized, CANCELLING");
  }
};

taskLib.sendGifts = async () => {
  const pogoState = stores.pogo.getState().data;
  const deviceRegion = pogoState.devices[0].region;

  const searchParams = new OptionalSearchParameters(deviceRegion, 0.95, true);

  const defaultWaitFor = (image) =>
    screen.waitFor(image, 2000, 200, searchParams);

  try {
    while (true) {
      const giftControlRegion = await defaultWaitFor(
        uiImages.gift.friendControlsSend
      );
      await mouse.setPosition(await randomPointIn(giftControlRegion));
      await mouse.leftClick();
      await sleep(1000);

      const giftRegion = await defaultWaitFor(uiImages.gift.send);
      await mouse.setPosition(await centerOf(giftRegion));
      await mouse.leftClick();
      await sleep(2000);

      const [sendButtonRegion, menuCloseRegion] = await Promise.all([
        defaultWaitFor(uiImages.gift.sendButton),
        defaultWaitFor(uiImages.general.menuCloseWhite),
      ]);

      await mouse.setPosition(await randomPointIn(sendButtonRegion));
      await mouse.leftClick();

      await mouse.setPosition(await randomPointIn(menuCloseRegion));
      await sleep(400);
      const loopCount = 1;
      for (let i = 0; i < loopCount; i++) {
        await sleep(100);
        await mouse.leftClick();
      }
      await sleep(1000);

      // swipe
      const friendControlsRegion = await defaultWaitFor(
        uiImages.general.friendControls
      );
      const { left, top, width, height } = friendControlsRegion;
      const rightSwipePoint = new Point(
        left + width,
        top + Math.floor(Math.random() * height)
      );
      const leftSwipePoint = new Point(
        left,
        top + Math.floor(Math.random() * height)
      );
      await mouse.setPosition(rightSwipePoint);
      await mouse.drag(straightTo(leftSwipePoint));
      await sleep(4000);
    }
  } catch (error) {
    logInspect(error);
  }
};

module.exports = taskLib;
