const {createStore, initStores} = require('@colorfy-software/zfy')
const tradeStore = require('./stores/tradeStore')
const stringsStore = require('./stores/stringsStore')
const pogoStore = require('./stores/pogoStore')
const hotkeysStore = require('./stores/hotkeysStore')

const {stores, useStores} = initStores([
  tradeStore,
  stringsStore,
  pogoStore,
  hotkeysStore,
])

module.exports = {
  stores,
  useStores
}
