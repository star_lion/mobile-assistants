const LocalStorage = require('node-localstorage').LocalStorage
const path = require('path')

const localStorage = new LocalStorage(path.join(__dirname, '../temp/pogoLocalStorage'))

module.exports = localStorage
