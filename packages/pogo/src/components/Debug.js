const {inspect} = require('util')
const React = require('react')
const {Box, Text, useFocus} = require('ink')
const activeWindow = require('active-win')
const {screen} = require('@nut-tree/nut-js')
const R = require('ramda')

const {Column, CommandBar, Command} = require('ink-components')
const {log, logInspect} = require('ink-components/LogView')
const {stores, useStores} = require('../useStores')
const {highlightDevices} = require('../taskLib')
const localStorage = require('../localStorage')

const {useState, useEffect} = React

const StringifyContainer = ({data, ...rest}) => (
  <Box {...rest}>
    <Text>{inspect(data, {depth: 3})}</Text>
  </Box>
)

const testFn = async () => {
  // const nutJs = require('@nut-tree/nut-js')
  // require('@nut-tree/template-matcher')

  // const uiImages = require('../uiImages')
  // const pogoStore = stores.pogo.getState()
  // const deviceRegion = pogoStore.data.devices[1].region
  // const oldHighlightDuration = screen.config.highlightDurationMs
  // screen.config.highlightDurationMs = 1000
  // try {
  //   await nutJs.screen.waitFor(
  //     uiImages.gift.receive,
  //     1000, 200,
  //     new nutJs.OptionalSearchParameters(deviceRegion, 0.95, true)
  //   )
  //   log(0)
  // } catch(error) {
  //   logInspect(error)
  // }
  // screen.config.highlightDurationMs = oldHighlightDuration

  const nutJs = require('@nut-tree/nut-js')
  require('@nut-tree/template-matcher')
  const {nutJs: {scaleRegion}} = require('utils')
  const uiImages = require('../uiImages')
  const pogoStore = stores.pogo.getState()
  const deviceRegion = pogoStore.data.devices[0].region
  const oldHighlightDuration = screen.config.highlightDurationMs
  screen.config.highlightDurationMs = 1000
  try {
    // const giftRegion = await nutJs.screen.waitFor(
    //   uiImages.gift.receive,
    //   3000, 200,
    //   new nutJs.OptionalSearchParameters(deviceRegion, 0.90, true)
    // )
    const matches = await screen.findAll(uiImages.gift.receive)
    log(matches)
    // const scaledGiftRegion = scaleRegion(giftRegion, .90, 1, 1)
    // await screen.highlight(scaledGiftRegion)
  } catch(error) {
    logInspect(error)
  }
  screen.config.highlightDurationMs = oldHighlightDuration

}

const Debug = ({}) => {
  const tradeStore = useStores('trade', data=>data)
  const stringsStore = useStores('strings', data=>data)
  const pogoStore = useStores('pogo', data=>data)
  const [currentWindow, setCurrentWindow] = useState(undefined)
  const [isDataVisible, setIsDataVisible] = useState(false)
  const dataDisplayOption = isDataVisible ? 'flex' : 'none'

  const combinedStores = {
    pogoStore,
    stringsStore,
    tradeStore: R.dissoc('steps', tradeStore)
  }

  useEffect(() => {
    const intervalId = setInterval(async () => {
      const {title} = await activeWindow()
      setCurrentWindow({title})
    }, 2000)
    return () => clearInterval(intervalId)
  }, [])

  return (
    <Column>
      <StringifyContainer
        display={dataDisplayOption}
        data={combinedStores}
      />
      <StringifyContainer
        display={dataDisplayOption}
        data={{
          pid: process.pid,
          ppid: process.ppid
        }}
      />
      <StringifyContainer
        display={dataDisplayOption}
        data={currentWindow}
      />
      <CommandBar>
        <Command
          id='testFunction'
          autoFocus={true}
          name='Test Function'
          onRun={testFn}
          hotkeysStore={stores.hotkeys}
        />
        <Command
          id='showData'
          autoFocus={true}
          name='Show Data'
          onRun={() => setIsDataVisible((state) => !state)}
          hotkeysStore={stores.hotkeys}
        />
        <Command
          id='highlightDevices'
          autoFocus={true}
          name='Highlight Devices'
          onRun={highlightDevices}
          hotkeysStore={stores.hotkeys}
        />
        <Command
          id='getMouseInfo'
          name='Get Mouse Info'
          onRun={() => {}}
          hotkeysStore={stores.hotkeys}
        />
      </CommandBar>
      <CommandBar>
        <Command
          id='clearLocalStorage'
          name='Clear Local Storage'
          onRun={() => localStorage.clear()}
          hotkeysStore={stores.hotkeys}
        />
        <Command
          id='resetStore'
          name='Reset Store'
          onRun={stores.reset}
          hotkeysStore={stores.hotkeys}
        />
      </CommandBar>
    </Column>
  )
}

module.exports = Debug
