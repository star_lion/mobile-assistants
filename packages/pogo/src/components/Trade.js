const {Box, Text, Newline} = require('ink')
const React = require('react')
const importJsx = require('import-jsx')
const {UiohookKey} = require('uiohook-napi')
const R = require('ramda')

const {Column, EnhancedTextInput, CommandBar, Command, DigitCarousel, TextCarousel, ArrowFocusManager} = require('ink-components')
const {arrayCycleForwards, arrayCycleBackwards} = require('utils')

const {useState, useEffect} = React
const {path} = R

const {getDevices, startTrading, storeDataCycle, increaseTradeCount, decreaseTradeCount} = require('../taskLib')
const {useStores, stores} = require('../useStores')

const tradeStore = stores.trade.getState()
const stringsStore = stores.strings.getState()

const updateFilterString = (newString) => {
  stringsStore.update(data => {
    if (newString.includes('<') || newString.includes('>')) return
    data.tradeFilter = newString.trim()
   })
}

const Trade = () => {
  const tradeFilterString = useStores('strings', data => data.tradeFilter)
  const {devices} = useStores('pogo', data => data)
  const {steps, count, executionModes, deviceModes} = useStores('trade', data => data)
  const stepLabels = steps.map(({number, description}) => `${number}: ${description}`)
  const isExecutionModeAutomatic = executionModes[0] == 'Automatic'
  const isDeviceModePair = deviceModes[0] == 'Pair'
  const connectedDevices = R.compose(
    R.values,
    R.map(R.prop('name')),
    R.reject(R.propSatisfies(R.isNil, 'region'))
  )(devices)

  const deviceNames = R.compose(
    R.values,
    R.map(R.prop('name'))
  )(devices)

  const textInputFocusIds = [
    'textInputTradeFilter'
  ]

  return (
    <Column>
      <Box>
        <Column width={20} borderStyle='single' alignItems='flex-end'>
          <Text>Connected Devices</Text>
          <Text>Current Step</Text>
          <Text>Device Mode</Text>
          {deviceModes[0] == 'Single'
            ? <Text>Preferred Device</Text>
            : <></>
          }

          <Text>Execution Mode</Text>

          <Box
            display={isExecutionModeAutomatic ? 'flex' : 'none'}
          >
            <Text>Count</Text>
          </Box>
          <Text>Filter String</Text>
        </Column>
        <Column flexGrow={1} borderStyle='single'>
          <ArrowFocusManager>
            <Text>{(connectedDevices.length > 0) ? connectedDevices.sort().join(', ') : 'None'}</Text>
            <TextCarousel
              id='tradeStep'
              autoFocus={true}
              values={stepLabels}
              onRight={() => storeDataCycle('trade', ['steps'], arrayCycleForwards)}
              onLeft={() => storeDataCycle('trade', ['steps'], arrayCycleBackwards)}
            />
            <TextCarousel
              id='tradeDeviceMode'
              values={deviceModes}
              onRight={() => storeDataCycle('trade', ['deviceModes'], arrayCycleForwards)}
              onLeft={() => storeDataCycle('trade', ['deviceModes'], arrayCycleBackwards)}
            />

            <TextCarousel
              id='tradePreferredDevice'
              values={deviceNames}
              isActive={isDeviceModePair ? false : true}
              display={isDeviceModePair ? 'none' : 'flex'}
              onRight={() => storeDataCycle('pogo', ['devices'], arrayCycleForwards)}
              onLeft={() => storeDataCycle('pogo', ['devices'], arrayCycleBackwards)}
            />

            <TextCarousel
              id='tradeExecutionMode'
              values={executionModes}
              onRight={() => storeDataCycle('trade', ['executionModes'], arrayCycleForwards)}
              onLeft={() => storeDataCycle('trade', ['executionModes'], arrayCycleBackwards)}
            />
            <DigitCarousel
              id='tradeCount'
              value={count}
              isActive={isExecutionModeAutomatic ? true : false}
              display={isExecutionModeAutomatic ? 'flex' : 'none'}
              onRight={increaseTradeCount}
              onLeft={decreaseTradeCount}
            />
            <EnhancedTextInput id='textInputTradeFilter' value={tradeFilterString} onChange={updateFilterString} />
          </ArrowFocusManager>
        </Column>
      </Box>
      <Column>
        <CommandBar alignItems='center'>
          <Text>Paste:</Text>
          <Command
            id='pasteTradeFilter'
            name='Trade Filter'
            prohibitedFocusIds={textInputFocusIds}
            onRun={(() => {})}
            hotkeysStore={stores.hotkeys}
          />
          <Command
            id='pasteTradeable'
            name='Tradeable'
            prohibitedFocusIds={textInputFocusIds}
            onRun={(() => {})}
            hotkeysStore={stores.hotkeys}
          />
          <Command
            id='pasteTraded'
            name='Traded Unnamed'
            prohibitedFocusIds={textInputFocusIds}
            onRun={(() => {})}
            hotkeysStore={stores.hotkeys}
          />
        </CommandBar>
        <CommandBar>
          <Command
            id='start'
            name={executionModes[0] == 'Manual' ? '[Alt-S]tep' : '[S]tart'}
            prohibitedFocusIds={textInputFocusIds}
            keyValidator={isExecutionModeAutomatic
              ? (event) => event.keycode == UiohookKey.S
              : (event) => event.altKey == true && event.keycode == UiohookKey.S
            }
            global={isExecutionModeAutomatic ? false : true}
            onRun={startTrading}
            hotkeysStore={stores.hotkeys}
          />
          <Command
            id='getDevices'
            name='[G]et Devices'
            prohibitedFocusIds={textInputFocusIds}
            keyValidator={(event) => event.keycode == UiohookKey.G}
            onRun={getDevices}
            hotkeysStore={stores.hotkeys}
          />
          <Command
            id='power'
            name='[P]ower'
            prohibitedFocusIds={textInputFocusIds}
            keyValidator={(event) => event.keycode == UiohookKey.P}
            onRun={(() => {})}
            hotkeysStore={stores.hotkeys}
          />
        </CommandBar>
      </Column>
    </Column>
  )
}

module.exports = Trade
