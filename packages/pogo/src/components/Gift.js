const {Box, Text} = require('ink')
const React = require('react')
const {UiohookKey} = require('uiohook-napi')
const R = require('ramda')
const importJsx = require('import-jsx')

const {Column, CommandBar, Command, TextCarousel, ArrowFocusManager} = importJsx('ink-components')
const {receiveGifts, sendGifts} = require('../taskLib')
const {arrayCycleForwards, arrayCycleBackwards} = require('utils')
const {storeDataCycle} = require('../taskLib')

const {stores, useStores} = require('../useStores')

const Gift = () => {
  const {devices} = useStores('pogo', data => data)
  const deviceNames = R.compose(
    R.values,
    R.map(R.prop('name'))
  )(devices)

  return (
    <Column>
      <Box>
        <ArrowFocusManager horizontalControl={false}>
          <Text>Preferred Device: </Text>
          <TextCarousel
            id='preferredDevice'
            values={deviceNames}
            onRight={() => storeDataCycle('pogo', ['devices'], arrayCycleForwards)}
            onLeft={() => storeDataCycle('pogo', ['devices'], arrayCycleBackwards)}
          />
        </ArrowFocusManager>
      </Box>
      <CommandBar>
        <Command
          id='receiveGifts'
          autoFocus={true}
          name='[R]eceive'
          keyValidator={(event) => event.keycode == UiohookKey.R}
          onRun={receiveGifts}
          hotkeysStore={stores.hotkeys}
        />
        <Command
          id='sendGifts'
          name='[S]end'
          keyValidator={(event) => event.keycode == UiohookKey.S}
          onRun={sendGifts}
          hotkeysStore={stores.hotkeys}
        />
        <Command
          id='power'
          name='[P]ower'
          keyValidator={(event) => event.keycode == UiohookKey.P}
          onRun={(() => {})}
          hotkeysStore={stores.hotkeys}
        />
      </CommandBar>
    </Column>
  )
}

module.exports = Gift
