const {createStore, initStores} = require('@colorfy-software/zfy')
const localStorage = require('../localStorage')

const initialState = {
  tradeFilter: 'trade&!mudkip',
}


const stringsStore = createStore('strings', initialState, {
  persist: {
    log: true,
    getStorage: () => localStorage
  }
})

module.exports = stringsStore
