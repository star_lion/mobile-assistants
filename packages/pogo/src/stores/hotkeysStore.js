const {createStore} = require('@colorfy-software/zfy')
const {uIOhook, UiohookKey} = require('uiohook-napi')
const R = require('ramda')

const initialState = {
  hotkeys: {}
}

const hotkeysStore = createStore('hotkeys', initialState, {})

uIOhook.on('keydown', (event) => {
  const {hotkeys} = hotkeysStore.getState().data
  Object.values(hotkeys).forEach((hotkey) => hotkey.callback(event))
})

module.exports = hotkeysStore
