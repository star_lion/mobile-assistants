const {createStore} = require('@colorfy-software/zfy')
const localStorage = require('../localStorage')
const R = require('ramda')

const initialState = {
  executionModes: ['Automatic', 'Manual'],
  deviceModes: ['Pair', 'Single'],
  steps: [
    {
      number: 1,
      description: 'Open trade view',
      taskName: 'openTradeView',
      animationTimes: {
        regular: 6,
        slow: 10
      }
    }, {
      number: 2,
      description: 'Make selection',
      taskName: 'selectMon',
      animationTimes: {
        regular: 2,
        slow: 4
      }
    }, {
      number: 3,
      description: 'Confirm selection',
      taskName: 'confirmMon',
      animationTimes: {
        regular: 2,
        slow: 4
      }
    }, {
      number: 4,
      description: 'Confirm trade',
      taskName: 'confirmTrade',
      animationTimes: {
        regular: 24,
        slow: 24
      }
    }, {
      number: 5,
      description: 'Close pokemon view',
      taskName: 'closeMon',
      animationTimes: {
        regular: 2,
        slow: 4
      }
    },
  ],
  animationTimeModifier: 0,
  count: 10,
}

const tradeStore = createStore('trade', initialState, {
  persist: {
    log: true,
    version: 0.5,
    getStorage: () => localStorage,
    partialize: (state) => {
      return R.compose(
        R.assocPath(['data', 'steps'], initialState.steps),
        R.assocPath(['data', 'count'], initialState.count)
      )(state)
    },
  }
})

module.exports = tradeStore
