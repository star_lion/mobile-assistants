const {createStore} = require('@colorfy-software/zfy')
const localStorage = require('../localStorage')
const R = require('ramda')

const initialState = {
tabs: ['Trade', 'Gift', 'Buddy', 'Friends', 'Battle', 'Settings', 'Debug'],
  activeTabIndex: 0,
  devices: [
    {
      name: 'Note 9',
      title: 'SM N960U1',
      serial: '299b2eedfb1c7ece',
      window: undefined,
      region: undefined
    },
    {
      name: 'Galaxy S7',
      title: 'SM G930T',
      serial: '3e6a404e',
      window: undefined,
      region: undefined
    }
  ],
}

const pogoStore = createStore('pogo', initialState, {
  persist: {
    log: true,
    version: 0.13,
    getStorage: () => localStorage,
    partialize: (state) => {
      return R.compose(
        R.assocPath(['data', 'devices', 0, 'window'], undefined),
        R.assocPath(['data', 'devices', 0, 'region'], undefined),
        R.assocPath(['data', 'devices', 1, 'window'], undefined),
        R.assocPath(['data', 'devices', 1, 'region'], undefined),
      )(state)
    }
  }
})

module.exports = pogoStore
