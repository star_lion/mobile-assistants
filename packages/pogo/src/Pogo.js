'use strict';
const React = require('react');
const {Box, Text, useFocus} = require('ink');
const {uIOhook} = require('uiohook-napi')
const importJsx = require('import-jsx')
const {useRehydrate} = require('@colorfy-software/zfy')

const {TitleBar, TabBar, Column} = require('ink-components')
const {LogView, log, logInPlace} = importJsx('ink-components/LogView')
const {getNextIndexFromIndex, getPreviousIndexFromIndex} = require('utils')
const {stores, useStores} = require('./useStores')
const {getDevices} = require('./taskLib')

const {useEffect} = React

const tabComponents = {
  Trade: importJsx('./components/Trade'),
  Gift: importJsx('./components/Gift'),
  Debug: importJsx('./components/Debug')
}

const activateNextTab = () => stores.pogo.getState().update(data => {
  data.activeTabIndex = getNextIndexFromIndex(data.activeTabIndex, data.tabs)
})

const activatePreviousTab = () => stores.pogo.getState().update(data => {
  data.activeTabIndex = getPreviousIndexFromIndex(data.activeTabIndex, data.tabs)
})

const Pogo = () => {
  const isRehydrated = useRehydrate([stores.trade])
  const {tabs, activeTabIndex} = useStores('pogo', data => data)
  const activeTab = tabs[activeTabIndex]

  useEffect(() => {
    log('Pogo initialization, ')
    getDevices()
    logInPlace('get devices, ')
    uIOhook.start()
    logInPlace('start uIOhook')
    return () => uIOhook.stop()
  }, [])

  const ActiveTabComponent = tabComponents[activeTab] != undefined
    ? tabComponents[activeTab]
    : () => <Box marginX={3}><Text>{activeTab} component undefined</Text></Box>

	return (
    <>
      <Box margin={2} borderColor='white' borderStyle='double'>
        <Column>
          <Box marginBottom={1} paddingX={2}>
            <TitleBar title='Pogo Assistant' />
          </Box>
          <Box marginX={3}>
            <TabBar
              tabNames={tabs}
              activeTabIndex={activeTabIndex}
              onNavigateRight={activateNextTab}
              onNavigateLeft={activatePreviousTab}
            />
          </Box>
          <Column marginX={3}>
            { isRehydrated
              ? <ActiveTabComponent />
              : <Text>Loading</Text>
            }
          </Column>
        </Column>
      </Box>
      <Box marginX={3}>
        <LogView />
      </Box>
    </>
	)
};

module.exports = Pogo;
