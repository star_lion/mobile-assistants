const {screen, OptionalSearchParameters, imageResource} = require('@nut-tree/nut-js')
require('@nut-tree/template-matcher') // THIS IS NEW
const path = require('path')
const {log} = require('ink-components/LogView')

const configureNutJsResourceDirectory = () => {
  screen.config.resourceDirectory = path.resolve(__dirname, '../data/images')
}
configureNutJsResourceDirectory()

const uiImages = {}

uiImages.general = {
  menuCloseGreen: imageResource('menu-close-green.png'),
  menuCloseWhite: imageResource('menu-close-white.png'),
  homeMenuPokeball: imageResource('home-menu-pokeball.png'),
  friendControls: imageResource('friend-controls.png'),
  friendsSearch: imageResource('friends-search.png'),
  inventoryFull: imageResource('inventory-full.png'),
}

uiImages.gift = {
  receive: imageResource('gift-receive.png'),
  receiveLimitMessage: imageResource('receive-limit-message.png'),
  open: imageResource('gift-open.png'),
  send: imageResource('gift-send.png'),
  sendButton: imageResource('gift-send-button.png'),
  friendControlsSend: imageResource('friend-controls-send.png')
}

uiImages.trade = {
  friendControlsTrade: imageResource('friend-controls-trade.png'),
  healthBar: imageResource('trade-health-bar.png'),
  nextButton: imageResource('trade-next-button.png'),
  confirmButton: imageResource('trade-confirm.png'),
}

uiImages.test = async () => {
  const matchRegion = await screen.find(imageResource('gift-receive.png'))
  await screen.highlight(matchRegion)
}

module.exports = uiImages
