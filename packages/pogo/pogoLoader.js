#!/usr/bin/env node
'use strict';
const React = require('react');
const importJsx = require('import-jsx');
const {render} = require('ink');
const meow = require('meow');

const Pogo = importJsx('./src/Pogo');

const cli = meow(`
	Usage
	  $ pogo

	Options
		--tab  Desired tab

	Examples
	  $ pogo --tab=gift
`);

console.clear()
render(React.createElement(Pogo, cli.flags), {patchConsole: true});
